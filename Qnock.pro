#-------------------------------------------------
#
# Project created by QtCreator 2009-11-16T21:25:33
#
#-------------------------------------------------

QT	       += network
TARGET		= Qnock
TEMPLATE	= app


MOC_DIR		= ./build/moc
OBJECTS_DIR	= ./build
UI_DIR		= ./build/ui
UI_HEADERS_DIR	= ./build/ui

SOURCES	+= ./src/main.cpp \
	   ./src/mainwindow.cpp \

HEADERS	+= ./src/mainwindow.h \

FORMS	+= ./ui/mainwindow.ui \
