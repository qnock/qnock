#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include <QInputDialog>
#include <QFileDialog>
#include <QUdpSocket>
#include <QHostAddress>
#include <QHostInfo>
#include <QMessageBox>
#include <QStringList>
#include <QString>
#include <QHash>
#include <QList>
#include <QFile>
#include <QTextStream>

namespace Ui
{
    class MainWindow;
}

class Sequence
{
public:
    Sequence() {}

    QString name;
    QString destination;
    QList<quint16> ports;
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void slotLoad();
    void slotUnload();
    void slotSave();
    void slotAbout();
    void slotSend();
    void slotInformationsModified();
    void slotSeqSelected(const QString&);

private:
    bool updatePortList();
    void addSeqence(const Sequence&);
    void addSeqence(const QString &name, const QString &to = QString(""), const QList<QString> &ports = QList<QString>());

    Ui::MainWindow *ui;
    QList<quint16> portsList;
    QUdpSocket *udpSocket;
    QHash<QString, Sequence> savedSeq;
    QFile *currentFile;

};

#endif // MAINWINDOW_H
